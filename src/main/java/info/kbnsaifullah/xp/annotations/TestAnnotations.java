package info.kbnsaifullah.xp.annotations;

/**
 * More about Annotations here:
 * https://dzone.com/articles/how-annotations-work-java
 * 
 * @author ksaifullah
 *
 */
public class TestAnnotations {

	@Test
	public void testPass() {
		// do something
	}

	@Test
	public void testFail() {
		throw new Error();
	}

	@Ignore
	@Test
	public void testIgnore() {
	}

	@ArrayAnnotation({ "Hi", "Bye" })
	public void testArrayAnnotation() {

	}

}
