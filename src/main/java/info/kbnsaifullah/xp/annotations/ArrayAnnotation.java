package info.kbnsaifullah.xp.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ArrayAnnotation {
	String[] value() default "";
}
