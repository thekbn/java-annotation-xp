package info.kbnsaifullah.xp.annotations;

public @interface SingleValueAnnotation {
	int value() default 0;
}
