package info.kbnsaifullah.xp.annotations.KUnit;

import java.lang.reflect.Method;

import info.kbnsaifullah.xp.annotations.ArrayAnnotation;
import info.kbnsaifullah.xp.annotations.Ignore;
import info.kbnsaifullah.xp.annotations.Test;
import info.kbnsaifullah.xp.annotations.TestAnnotations;

public class KUnit {
	public static void main(String... args) {

		int passed = 0, failed = 0, count = 0, ignore = 0;

		Class<TestAnnotations> classUnderTest = TestAnnotations.class;

		for (Method method : classUnderTest.getDeclaredMethods()) {
			if (method.getAnnotation(Test.class) != null) {
				if (method.getAnnotation(Ignore.class) != null) {
					System.out.printf("%s - Test '%s' - ignored%n", ++count, method.getName());
					ignore++;
					continue;
				}
				try {
					method.invoke(classUnderTest.newInstance());
					System.out.printf("%s - Test '%s' - passed %n", ++count, method.getName());
					passed++;
				} catch (Throwable ex) {
					System.out.printf("%s - Test '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
					failed++;
				}

			}
			
			ArrayAnnotation at = (ArrayAnnotation) method.getAnnotation(ArrayAnnotation.class);
			if(at != null) {
				for(String v : at.value()) {
					System.out.println(v);
				};
			}
		}

		System.out.printf("%nResult : Total : %d, Passed: %d, Failed %d, Ignore %d%n", count, passed, failed, ignore);

	}
}
